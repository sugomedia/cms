<h1>Hír módosítása</h1>
<hr>
<?php
    logincheck();
    $hirID = $_GET['id'];

    // ha a módosítás gombra kattintottunk
    if (isset($_POST['modosit']))
    {
        $cim = escapeshellcmd($_POST['cim']);
        $leiras = escapeshellcmd($_POST['leiras']);

        if (empty($cim) || empty($leiras))
        {
            echo '<em>Hiba! Nem adtál meg minden adatot!</em>';
        }
        else
        {
            $uID = $_SESSION['uID'];
            dbquery("UPDATE hirek SET cim='$cim', leiras='$leiras', datum=CURRENT_TIMESTAMP WHERE ID=".$hirID, $kapcsolat);
            header("location: index.php?pg=hirek");
        }
    }

    // ha a törlés gombrta kttintottunk
    if (isset($_POST['torol']))
    {
        dbquery("DELETE FROM hirek WHERE ID=".$hirID, $kapcsolat);
        header("location: index.php?pg=hirek");  
    }

    $result = dbquery("SELECT * FROM hirek WHERE ID=".$hirID, $kapcsolat);
    $hir = mysqli_fetch_assoc($result);

    echo '<form action="index.php?pg=hirmod&id='.$hirID.'" method="post">
        <div class="form-group">
            <label for="cim">A hír címe:</label><br>
            <input type="text" name="cim" placeholder="A hír címe" value="'.$hir['cim'].'">
        </div>
        <div class="form-group">
            <label for="leiras">Leírás:</label><br>
            <textarea name="leiras" class="leiras">'.$hir['leiras'].'</textarea>
        </div>
        <div class="form-group">
            <br><br>
            <input type="submit" name="modosit" value="Hír módosítása">
            <br><br>
            <input type="submit" name="torol" value="Hír törlése">
        </div>   
    </form>';
?>


