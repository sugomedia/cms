<h1>Regisztráció</h1>
<hr>

<?php
    // rákattintott-e a regisztráció gombra? 
    if (isset($_POST['reg']))
    {
        // az inputok megtisztítása az escape karakterektől (SQL injection ellen)
        $fnev = escapeshellcmd($_POST['fnev']);
        $email = escapeshellcmd($_POST['email']);
        $pass1 = escapeshellcmd($_POST['pass1']);
        $pass2 = escapeshellcmd($_POST['pass2']);
        // megadott-e minden adatot?
        if (empty($fnev) || empty($email) || empty($pass1) || empty($pass2))
        {
            echo '<em>Hiba! Nem adtál meg minden adatot!</em>';
        }
        else
        {
            // egyeznek-e a megadott jelszavak?
            if ($pass1 != $pass2)
            {
                echo '<em>Hiba! A megadott jelszavak nem egyeznek!</em>';
            }
            else
            {
                // van-e már ilyen email címmel regisztráció?
                $result = dbquery("SELECT ID FROM felhasznalok WHERE email='$email'", $kapcsolat);
                if (mysqli_num_rows($result) != 0)
                {
                    echo '<em>Hiba! Van már ilyen regisztráció! Válassz másik em-ail címet!</em>';
                }
                else
                {
                    $pass1 = SHA1($pass1);
                    dbquery("INSERT INTO felhasznalok VALUES(
                        null,
                        '$fnev',
                        '$email',
                        '$pass1',
                        CURRENT_TIMESTAMP,
                        null,
                        1
                    )", $kapcsolat);
                    echo 'A regisztráció sikeres!';
                }
            }
        }
    }
?>

<form method="POST" action="index.php?pg=regisztracio">
    <label for="fnev">Felhasználónév:</label><br>
    <input type="text" name="fnev"><br>
    <br>
    <label for="email">E-mail cím:</label><br>
    <input type="email" name="email"><br>
    <br>
    <label for="pass1">Jelszó:</label><br>
    <input type="password" name="pass1"><br>
    <br>
    <label for="pass2">Jelszó megerősítése:</label><br>
    <input type="password" name="pass2"><br>
    <br>
    <input type="submit" value="Regisztráció elküldése" name="reg">
</form>