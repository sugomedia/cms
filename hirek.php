<h1>Hírek</h1>
<hr>
<?php
    if (isset($_SESSION['uID']))
    {
        echo '<a href="index.php?pg=hirfelv">Új hír felvétele...</a><br><br>';
    }

    $hirek = dbquery("SELECT hirek.ID AS ID, cim, leiras, datum, nev FROM hirek 
    INNER JOIN felhasznalok ON felhasznalok.ID = hirek.felhID
    ORDER BY datum DESC", $kapcsolat);
   
    if (mysqli_num_rows($hirek) == 0)
    {
        echo 'Jelenleg nincsenek híreink!';
    }
    else
    {
        while($hir = mysqli_fetch_assoc($hirek))
        {
            echo '<div class="hirbox">
            <h3>'.$hir['cim'].'<span>';

            // ha be vagyunk jelentkezve, akkor kirakunk egy szerkesztés linket
            // nl2br($hir['leiras']) -> a leírás mező tartalmát a html formázásokkal együtt jeleníti meg
            if (isset($_SESSION['uID']))
            {
                echo ' <a href="index.php?pg=hirmod&id='.$hir['ID'].'">[ szerkesztés ]</a>';
            }
            echo '</span></h3>
            <p>'.nl2br($hir['leiras']).'</p>
            <h5>'.$hir['datum'].' - '.$hir['nev'].'</h5>
            </div>';
        }
    }
?>