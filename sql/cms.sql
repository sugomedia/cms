-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Gép: localhost
-- Létrehozás ideje: 2020. Már 13. 15:33
-- Kiszolgáló verziója: 10.4.6-MariaDB
-- PHP verzió: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `113szfe_cms`
--
CREATE DATABASE IF NOT EXISTS `113szfe_cms` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
USE `113szfe_cms`;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `felhasznalok`
--

CREATE TABLE `felhasznalok` (
  `ID` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `jelszo` varchar(40) COLLATE utf8_hungarian_ci NOT NULL,
  `regdatum` datetime NOT NULL,
  `utbelep` datetime DEFAULT NULL,
  `statusz` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `felhasznalok`
--

INSERT INTO `felhasznalok` (`ID`, `nev`, `email`, `jelszo`, `regdatum`, `utbelep`, `statusz`) VALUES
(1, 'Próba felhasználó', 'proba@cms.hu', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2020-02-21 00:00:00', '2020-02-21 00:00:00', 1),
(2, 'Próba felhasználó 2', 'proba2@cms.hu', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2020-02-21 15:33:36', NULL, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE `hirek` (
  `ID` int(11) NOT NULL,
  `datum` datetime NOT NULL,
  `cim` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `felhID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `hirek`
--

INSERT INTO `hirek` (`ID`, `datum`, `cim`, `leiras`, `felhID`) VALUES
(1, '2020-03-06 15:04:11', 'Ez az első hírünk.', 'nfja hfjsdah fkjasd hfsda fj,as fhsa dfha sfjha sdfjkhasjkdfh asdjkfh askjd fhasjkdf hjaksdfhjkash fjashdf asdf\r\nkajhdf iahsjdf hasjkdfh ajskdf j,sah fagsdf asfhgas dhfg asfg ashfg  fdh asdf\r\nasd,hfja sgdf,hag s gashfg ashdfgasg fhsa gdjfasgdhf gsadf gasd fas gf dsfjsadf', 1),
(2, '2020-03-06 15:05:06', 'Született egy kis panda', 'djfh s,djf gajskh fdjkasd hfjaks hfhasgdfjkasdhfjk asdfjh adskfjh sadfjkhasd,fkjh asdjkfh asdjkf hasjdfkh sajdkfh sdkjh dsfkjh asdf, asdfkjh asdjkfh asdkjfh asdfjbasdjf asdkjfh askjdfhsa jkdfhaskdjf hjaksdf kjhsdf ajkfdh asjkdf gasjdfg jdsgf asdg fkaj hdsgfjkah fgkjg fksau', 1),
(4, '2020-03-13 15:25:38', 'A digitális tér teljes újraszabályozására készül a magyar kormány', '<p style=\"text-align: justify;\">Az</p>\r\n<h2 style=\"text-align: justify;\"><strong>Igazs&aacute;g&uuml;gyi Miniszt&eacute;rium</strong></h2>\r\n<p style=\"text-align: justify;\">l&eacute;trehozta a Digit&aacute;lis Szabads&aacute;g Munkacsoportot, amely a nemzetk&ouml;zi technol&oacute;giai c&eacute;gek műk&ouml;d&eacute;s&eacute;nek &aacute;tl&aacute;that&oacute;v&aacute; t&eacute;tel&eacute;t tűzte ki c&eacute;lul &ndash; k&ouml;z&ouml;lte Varga Judit igazs&aacute;g&uuml;gyi miniszter az MTI besz&aacute;mol&oacute;ja szerint az M1 ked<strong>d reggeli műsor&aacute;ban Adatokat,</strong> inform&aacute;ci&oacute;kat gyűjtenek r&oacute;lunk, elemzik, befoly&aacute;solj&aacute;k fogyaszt&aacute;si szok&aacute;sainkat. Ha nem fizetsz egy szolg&aacute;ltat&aacute;s&eacute;rt, lehet, hogy te magad vagy a szolg&aacute;ltat&aacute;s. Kattint&aacute;sainkkal magunkr&oacute;l adhatunk ki inform&aacute;ci&oacute;kat. Korunk egyik legnagyobb kih&iacute;v&aacute;sa, hogy k&eacute;pesek lesz&uuml;nk-e uralni a technik&aacute;t, vagy a technika uralja majd &eacute;let&uuml;nket &ndash; mondta a miniszter.</p>\r\n<p style=\"text-align: center;\">A munkacsoport feladata, hogy a technol&oacute;giai &oacute;ri&aacute;sc&eacute;gek műk&ouml;d&eacute;s&eacute;t a demokratikus alapjogok, a szem&eacute;lyes szabads&aacute;gjogok &eacute;s a jog&aacute;llami műk&ouml;d&eacute;se szempontj&aacute;b&oacute;l vizsg&aacute;lja. Olyan konkr&eacute;t k&eacute;rd&eacute;sekre is v&aacute;laszt keresnek, hogy milyen jogorvoslati lehetős&eacute;ge van annak, akinek t&ouml;rlik a platformj&aacute;t, vagy, hogy mennyire &aacute;tl&aacute;that&oacute; a hirdet&eacute;sek megjelen&eacute;s&eacute;nek algoritmusa &ndash; k&ouml;z&ouml;lte Varga Judit.</p>\r\n<p>Hozz&aacute;tette: miut&aacute;n orsz&aacute;ghat&aacute;rokon &aacute;tny&uacute;l&oacute; tev&eacute;kenys&eacute;gről van sz&oacute;, nemzetk&ouml;zi &ouml;sszefog&aacute;sban kell gondolkodni, ez&eacute;rt az egyeztet&eacute;sekbe bevonj&aacute;k a technol&oacute;giai &oacute;ri&aacute;sc&eacute;geket. A munkacsoport weboldal&aacute;n megtal&aacute;lhat&oacute; egy r&ouml;vid &ouml;sszefoglal&oacute; a c&eacute;lokr&oacute;l. A n&eacute;h&aacute;ny oldalas dokumentum, a &bdquo;Feh&eacute;r K&ouml;nyv&rdquo; meg&aacute;llap&iacute;t&aacute;sainak legfontosabbjai k&ouml;z&uuml;l n&eacute;h&aacute;nyat emel&uuml;nk ki: a nagy platformokat műk&ouml;dtető techc&eacute;gek a felhaszn&aacute;l&aacute;si felt&eacute;teleket maguk szabj&aacute;k meg, &eacute;s ezzel vesz&eacute;lyeztethetik a sz&oacute;l&aacute;sszabads&aacute;g &eacute;rv&eacute;nyes&uuml;l&eacute;s&eacute;t:</p>\r\n<p style=\"text-align: right;\">&bdquo;Mindez nemzeti szuverenit&aacute;st &eacute;rintő probl&eacute;m&aacute;t is felvet (pszeudojogrendszer)&rdquo; a &bdquo;fake news&rdquo; probl&eacute;m&aacute;ja, ennek hat&aacute;sai a tiszta v&aacute;laszt&aacute;sokra a k&ouml;z&ouml;ss&eacute;gi platformokon neh&eacute;z azonos&iacute;tani a tartalmak k&ouml;zz&eacute;tevőit, &iacute;gy adott esetben korl&aacute;tozott a jogi szankci&oacute; lehetős&eacute;ge neh&eacute;z a multinacion&aacute;lis, d&ouml;ntően amerikai sz&eacute;khelyű, online tev&eacute;kenys&eacute;get is folytat&oacute; c&eacute;gekkel szemben adatv&eacute;delmi &uuml;gyekben a jogorvoslat &eacute;rv&eacute;nyes&iacute;t&eacute;se az adatgyűjt&eacute;ssel &eacute;s -elemz&eacute;ssel l&eacute;trehozott szem&eacute;lyis&eacute;gprofilok probl&eacute;m&aacute;ja a nagy online platformok szoftveres műk&ouml;d&eacute;se nem el&eacute;gg&eacute; &aacute;tl&aacute;that&oacute;: &bdquo;az online platformokon t&ouml;megesen meghozott d&ouml;nt&eacute;sekben meghat&aacute;roz&oacute; szerepet j&aacute;tsz&oacute; algoritmusok (illetve a platform műk&ouml;d&eacute;s&eacute;t szab&aacute;lyoz&oacute; &raquo;k&oacute;dok&laquo;) műk&ouml;d&eacute;s&eacute;nek &aacute;tl&aacute;that&oacute;s&aacute;ga n&eacute;lk&uuml;l nem hat&aacute;rozhat&oacute; meg, hogy a felhaszn&aacute;l&oacute;k sz&aacute;m&aacute;ra egyes tartalmak előt&eacute;rbe helyez&eacute;s&eacute;re, m&iacute;g m&aacute;sok t&ouml;rl&eacute;s&eacute;re vagy h&aacute;tt&eacute;rbe szor&iacute;t&aacute;s&aacute;ra milyen elvek, szempontok alapj&aacute;n ker&uuml;l sor&rdquo; ad&oacute;z&aacute;si gondok az online platformokon neh&eacute;zs&eacute;gekbe &uuml;tk&ouml;zik a gyűl&ouml;letbesz&eacute;d, illetve az egy&eacute;b bűncselekm&eacute;nyek &uuml;ld&ouml;z&eacute;se &bdquo;Nemzeti szuverenit&aacute;st &eacute;rintő k&eacute;rd&eacute;sek: - az online techc&eacute;gek digit&aacute;lis kv&aacute;zi-szuverenit&aacute;s&aacute;nak k&eacute;rd&eacute;se&rdquo;</p>', 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
