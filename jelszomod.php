<h1>Jelszó módosítás</h1>
<hr>

<?php
    logincheck();
    if (isset($_POST['passmod']))
    {
        $oldpass = escapeshellcmd($_POST['oldpass']);
        $newpass1 = escapeshellcmd($_POST['newpass1']);
        $newpass2 = escapeshellcmd($_POST['newpass2']);

        if (empty($oldpass) || empty($newpass1) || empty($newpass2))
        {
            echo '<em>Hiba! Nem adtál meg minden adatot!</em>';
        }
        else
        {
            if ($newpass1 != $newpass2)
            {
                echo '<em>Hiba! A megadott új jelszavak nem egyeznek!</em>';
                $_POST['newpass1'] = '';
                $_POST['newpass2'] = '';
            
            }
            else
            {
                $result = dbquery("SELECT jelszo FROM felhasznalok WHERE ID=".$_SESSION['uID'],  $kapcsolat);
                $user = mysqli_fetch_assoc($result);
                if ($user['jelszo'] != SHA1($oldpass))
                {
                    echo '<em>Hiba! A jelenlegi jelszó nem megfelelő!</em>';
                    $_POST['oldpass'] = '';
                }
                else
                {
                    $newpass1 = SHA1($newpass1);
                    dbquery("UPDATE felhasznalok SET jelszo = '$newpass1' WHERE ID=".$_SESSION['uID'], $kapcsolat);
                    echo 'A jelszó módosítás sikerült!';
                    $_POST['oldpass'] = '';
                    $_POST['newpass1'] = '';
                    $_POST['newpass2'] = '';
                }
            }
        }
    }
    else
    {
        $_POST['oldpass'] = '';
        $_POST['newpass1'] = '';
        $_POST['newpass2'] = '';
    }

    echo '<form method="POST" action="index.php?pg=jelszomod">
        <input type="password" name="oldpass" placeholder="Jelenlegi jelszó" value="'.$_POST['oldpass'].'">
        <br><br>
        <input type="password" name="newpass1" placeholder="Új jelszó" value="'.$_POST['newpass1'].'">
        <br><br>
        <input type="password" name="newpass2" placeholder="Új jelszó megerősítése" value="'.$_POST['newpass2'].'">
        <br><br>
        <input type="submit" value="Módosítás" name="passmod">
    </form>';
?>


