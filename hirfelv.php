<h1>Új hír felvétele</h1>
<hr>

<?php

    logincheck();

    if (isset($_POST['felvesz']))
    {
        $cim = escapeshellcmd($_POST['cim']);
        $leiras = escapeshellcmd($_POST['leiras']);

        if (empty($cim) || empty($leiras))
        {
            echo '<em>Hiba! Nem adtál meg minden adatot!</em>';
        }
        else
        {
            $uID = $_SESSION['uID'];
            dbquery("INSERT INTO hirek VALUES(null, CURRENT_TIMESTAMP, '$cim','$leiras', $uID)", $kapcsolat);
            header("location: index.php?pg=hirek");
        }
    }
?>

<form method="POST" action="index.php?pg=hirfelv">
    <div class="form-group">
        <label for="cim">A hír címe:</label><br>
        <input type="text" name="cim" placeholder="A hír címe">
    </div>
    <div class="form-group">
        <label for="leiras">Leírás:</label><br>
        <textarea name="leiras" class="leiras"></textarea>
    </div>
    <div class="form-group">
        <input type="submit" name="felvesz" value="Hír felvétele">       
    </div>   
</form>