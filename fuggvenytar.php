<?php
    // adatbázis kapcsolódás
    function dbconnect($dbhost, $dbname, $dbuser, $dbpass)
    {
        if (!($link = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname)))
        {
            die("Hiba a kiszolgálóhoz történő csatlakozáskor! Hibakód: ".mysqli_connect_errno());
        }
        else
        {
            mysqli_query($link, "SET NAMES utf8");
          //  mysqli_set_charset($link, "utf8");
            return $link;
        }
    }

    // adatbázis lekérdezés
    function dbquery($sql, $connection)
    {
        if (!($result = mysqli_query($connection, $sql)))
        {
            die("Hiba az SQL lekérdezés végrehajtása közben! <br><br>" + $sql);
        }
        else
        {
            return $result;
        }
    }

    // bejelentkezés ellenőrzés
    function logincheck()
    {
        if (!isset($_SESSION['uID']))
        {
            header("location: index.php");
        }
    }


    function sendmail($cimzett, $cimzettemail, $kuldo, $kuldoemail, $targy, $body, $csatolmany)
      {
          require('PHPMailer/class.phpmailer.php');
          $mail = new PHPMailer();
          $mail->charSet = "UTF-8";
              $mail->From = $kuldoemail;
          $mail->FromName = $kuldo;
              $mail->Mailer = "mail";
    
          $mail->AddAddress($cimzettemail,$cimzett);
          $mail->AddReplyTo($kuldoemail,$kuldo);
          $mail->WordWrap = 50; // set word wrap
          if(!empty($csatolmany))
          {
            if(is_array($csatolmany))
            {
                foreach($csatolmany as $csat)
                {
                    $mail->AddAttachment($csat); // attachment
                }
            }
            else
            {
                $mail->AddAttachment($csatolmany); // attachment
            }
          }
          
          
          $mail->IsHTML(true); // send as HTML
        
          $mail->Subject = $targy;
        
          $mail->Body = $body;      //HTML Body
          
          
          $mail->AltBody = strip_tags($body);     //Plain Text Body
          if(!$mail->Send())
          {
            return $mail->ErrorInfo;
          } 
          else 
          {
            return 'OK';
          }
      }
?>