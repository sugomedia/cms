<?php
    if (!isset($_SESSION['uID']))
    {
        if (isset($_POST['login']))
        {
            $email = escapeshellcmd($_POST['email']);
            $jelszo = escapeshellcmd($_POST['jelszo']);
            if (empty($email) || empty($jelszo))
            {
                echo '<em>Hiba! Nem adtál meg minden adatot!</em>';
            }
            else
            {
                $result = dbquery("SELECT * FROM felhasznalok WHERE email='$email'", $kapcsolat);
                if (mysqli_num_rows($result) == 0)
                {
                    echo '<em>Hiba! Nem regisztrált e-mail cím!</em>';
                }
                else
                {
                    $felhasznalo = mysqli_fetch_assoc($result);
                    if ($felhasznalo['statusz'] == 0)
                    {
                        echo '<em>Hiba! Tiltott felhasználó!</em>';
                    }
                    else
                    {
                        // a felhasználók táblából kiolvasott jelszó HASH-t összehasonlítjuk az űrlapban megadott jelszóval, amit SHA1-el le kell kódolnunk, hogy összehasonlíthassuk
                        if ($felhasznalo['jelszo'] != SHA1($jelszo))
                        {
                            echo '<em>Hiba! Hibás jelszó!</em>';
                        }
                        else
                        {
                            // ha minden oké, beléphet, létrehozunk egy munkamenet változót és átadjuk neki a bejelentkező felhasználó ID-jét
                            $_SESSION['uID'] = $felhasznalo['ID'];
                            $_SESSION['uName'] = $felhasznalo['nev'];
                            $_SESSION['uMail'] = $felhasznalo['email'];
                            header("location: index.php?pg=belepes");
                        }
                    }
                }
            }
        }
        echo '
        <h1>Belépés</h1>
        <hr>
        <form method="POST" action="index.php?pg=belepes">
            <label for="email">E-mail cím:</label><br>
            <input type="email" name="email">
            <br>
            <label for="jelszo">Jelszó:</label><br>
            <input type="password" name="jelszo">
            <br><br>
            <input type="submit" value="Belépés" name="login">
        </form>';
    }
    else
    {
        if (isset($_POST['logout']))
        {
            // megszüntetjük a munkamenet változót
            unset($_SESSION['uID']);
            unset($_SESSION['uName']);
            unset($_SESSION['uMail']);
            header("location: index.php?pg=belepes");
        }
        echo '
        <h1>Kilépés</h1>
        <hr>
        <form method="POST" action="index.php?pg=belepes">
        <h3>'.$_SESSION['uName'].'</h3>
        <h4>'.$_SESSION['uMail'].'</h4>
        <input type="submit" value="Kilépés" name="logout">';
    }

?>


