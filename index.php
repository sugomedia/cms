<!DOCTYPE html>
<?php
    session_start();
    include("adatok.php");
    include("fuggvenytar.php");
    $kapcsolat = dbconnect($dbhost, $dbname, $dbuser, $dbpass);
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $pagename; ?>
    </title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/cms.css">

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
        selector: '.leiras'
      });
    </script>

</head>
<body>
    <div  id="help">
        <div class="visible-xs">XS</div>
        <div class="visible-sm">SM</div>
        <div class="visible-md">MD</div>
        <div class="visible-lg">LG</div>
    </div>

    <div id="container" class="container">
        <div id="header" class="col-xs-12">
            <a href="index.php">
            <img src="https://fakeimg.pl/150x75/eae0d0/282828/?text=<?php echo $pagename; ?>">            
            </a>
        </div>
        <div id="menu" class="col-xs-12 col-md-3">
            <?php include("menu.php");?>
        </div>
        <div id="content" class="col-xs-12 col-md-9">
            <?php include("betolto.php");?>
        </div>
        <div id="footer" class="col-xs-12">
        <?php echo $company.' - '.$author; ?>
        </div>
    </div>
</body>
</html>